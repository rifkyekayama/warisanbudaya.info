<header id="header" class="navbar-fixed-top header2" role="banner">
	<div class="container">
		<div class="row">
			<!-- Logo start -->
			<div class="navbar-header">
			    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			    </button>
			    <div class="navbar-brand">
				    <a href="{{ URL::to('/')}}">
				    	<img class="img-responsive" src="{{asset('assets/frontend/images/logo.png')}}" alt="logo">
				    </a> 
			    </div>                   
			</div><!--/ Logo end -->
			<nav class="collapse navbar-collapse clearfix" role="navigation">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="{{ URL::to('/') }}">Home</a></li>
        			<li><a href="{{ URL::to('cari_budaya') }}">Cari Budaya</a></li>
        			<li><a href="{{ URL::to('register') }}">Daftar</a></li>
                </ul>
			</nav><!--/ Navigation end -->
		</div><!--/ Row end -->
	</div><!--/ Container end -->
</header><!--/ Header end -->