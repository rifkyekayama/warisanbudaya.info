<section id="home" class="no-padding">
	<div id="main-slide" class="ts-flex-slider">
		<div class="flexSlideshow flexslider">
			<ul class="slides">

				<li>
					<div class="overlay2">
						<img class="" src="{{asset('assets/frontend/images/slider/slider_1.jpg')}}" alt="slider">
					</div>
					<div class="flex-caption slider-content">
                        <div class="col-md-12 text-center">
                    		<h2 class="animated2">
                        		Budaya Indonesia
                        	</h2>
                        </div>
                    </div>
				</li>

				<li>
					<div class="overlay2">
						<img class="" src="{{asset('assets/frontend/images/slider/slider_2.jpg')}}" alt="slider">
					</div>
					<div class="flex-caption slider-content">
                        <div class="col-md-12 text-center">
                            <h2 class="animated4">
                                Warisan Budaya Indonesia
                            </h2>	     
                        </div>
                    </div>
				</li>

				<li>
					<div class="overlay2">
						<img class="" src="{{asset('assets/frontend/images/slider/slider_3.jpg')}}" alt="slider">
					</div>
					<div class="flex-caption slider-content">
                        <div class="col-md-12 text-center">
                            <h2 class="animated7">
                                Berjaya di Tanah Legenda
                            </h2>		    
                        </div>
                    </div>
				</li>

                <li>
                    <div class="overlay2">
                        <img class="" src="{{asset('assets/frontend/images/slider/slider_4.jpg')}}" alt="slider">
                    </div>
                    <div class="flex-caption slider-content">
                        <div class="col-md-12 text-center">
                            <h2 class="animated7">
                                Lestarikan Budaya Indonesia
                            </h2>          
                        </div>
                    </div>
                </li>

                <li>
                    <div class="overlay2">
                        <img class="" src="{{asset('assets/frontend/images/slider/slider_5.jpg')}}" alt="slider">
                    </div>
                    <div class="flex-caption slider-content">
                        <div class="col-md-12 text-center">
                            <h2 class="animated7">
                                Cintai Indonesia
                            </h2>    
                        </div>
                    </div>
                </li>

                <li>
                    <div class="overlay2">
                        <img class="" src="{{asset('assets/frontend/images/slider/slider_6.jpg')}}" alt="slider">
                    </div>
                    <div class="flex-caption slider-content">
                        <div class="col-md-12 text-center">
                            <h2 class="animated7">
                                Mari Lestarikan Budaya Indonesia
                            </h2>    
                        </div>
                    </div>
                </li>
                
			</ul>
		</div>
	</div><!--/ Main slider end -->    	
</section> <!--/ Slider end -->