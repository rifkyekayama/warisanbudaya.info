<section class="ts_counter no-padding">
	<div class="container-fluid">
		<div class="row facts-wrapper wow fadeInLeft text-center">

			<div class="facts one col-md-6 col-sm-6">
				<span class="facts-icon"><i class="fa fa-user"></i></span>
				<div class="facts-num">
					<span class="counter">1200</span>
				</div>
				<h3>Uploaders</h3> 
			</div>

			<div class="facts two col-md-6 col-sm-6">
				<span class="facts-icon"><i class="fa fa-institution"></i></span>
				<div class="facts-num">
					<span class="counter">2010</span>
				</div>
				<h3>Data Budaya</h3> 

			</div>
			{{-- 
			<div class="facts three col-md-3 col-sm-6">
				<span class="facts-icon"><i class="fa fa-suitcase"></i></span>
				<div class="facts-num">
					<span class="counter">869</span>
				</div>
				<h3>Projects</h3> 
			</div>

			<div class="facts four col-md-3 col-sm-6">
				<span class="facts-icon"><i class="fa fa-trophy"></i></span>
				<div class="facts-num">
					<span class="counter">76</span>
				</div>
				<h3>Awwards</h3> 
			</div>
			--}}
		</div>
	</div><!--/ Container end -->
</section><!--/ Counter end -->	