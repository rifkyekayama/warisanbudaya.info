<!-- initialize jQuery Library -->
{{HTML::script('assets/frontend/js/jquery.js')}}
<!-- Bootstrap jQuery -->
{{HTML::script('assets/frontend/js/bootstrap.min.js')}}
<!-- Style Switcher -->
{{HTML::script('assets/frontend/js/style-switcher.js')}}
<!-- Owl Carousel -->
{{HTML::script('assets/frontend/js/owl.carousel.js')}}
<!-- PrettyPhoto -->
{{HTML::script('assets/frontend/js/jquery.prettyPhoto.js')}}
<!-- Bxslider -->
{{HTML::script('assets/frontend/js/jquery.flexslider.js')}}
<!-- Owl Carousel -->
{{HTML::script('assets/frontend/js/cd-hero.js')}}
<!-- Isotope -->
{{HTML::script('assets/frontend/js/isotope.js')}}
{{HTML::script('assets/frontend/js/ini.isotope.js')}}
<!-- Wow Animation -->
{{HTML::script('assets/frontend/js/wow.min.js')}}
<!-- SmoothScroll -->
{{HTML::script('assets/frontend/js/smoothscroll.js')}}
<!-- Eeasing -->
{{HTML::script('assets/frontend/js/jquery.easing.1.3.js')}}
<!-- Counter -->
{{HTML::script('assets/frontend/js/jquery.counterup.min.js')}}
<!-- Waypoints -->
{{HTML::script('assets/frontend/js/waypoints.min.js')}}
<!-- Google Map API Key Source -->
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<!-- For Google Map -->
{{HTML::script('assets/frontend/js/gmap3.js')}}
<!-- Doc http://www.mkyong.com/google-maps/google-maps-api-hello-world-example/ -->
<!-- Template custom -->
{{HTML::script('assets/frontend/js/custom.js')}}