<!DOCTYPE html>
<html lang="en">
<head>

	<!-- Basic Page Needs
	================================================== -->
	<meta charset="utf-8">
    <title>Warisan Budaya | Indonesia</title>
    <meta name="description" content="">	
	<meta name="author" content="">

	<!-- Mobile Specific Metas
	================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="{{ asset('favicon.png') }}">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon/favicon-144x144.html">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon/favicon-72x72.html">
	<link rel="apple-touch-icon-precomposed" href="img/favicon/favicon-54x54.html">
	
	<!-- CSS
	================================================== -->
	@include('frontend.includes.css')
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->

</head>
	
<body>

	<div class="body-inner">
	<!-- Header start -->
	@include('frontend.includes.header')

	<!-- Slider start -->
	@yield('slider')

	<!-- Content Strat -->
	@yield('content')

	<!-- Footer start -->
	@include('frontend.includes.footer')	

	<!-- Javascript Files
	================================================== -->
	@include('frontend.includes.javascript')

	</div><!-- Body inner end -->
</body>
</html>