<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from demo.themegret.com/html/island/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 11 Sep 2015 15:57:40 GMT -->
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Cari Budaya</title>

	<link rel="shortcut icon" href="{{ asset('favicon.png') }}">	

	<link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet'>
	<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
	{{ HTML::style('assets/searching/css/font-awesome.min.css')}}
	{{ HTML::style('assets/searching/css/bootstrap.css')}}
	{{ HTML::style('assets/searching/css/search.css')}}
	{{ HTML::style('assets/searching/css/style.css')}}
	
</head>
<body>


	<!-- Start preloader -->
		<div class="preloader">
			<div class="loader fa-spin"></div>
		</div>
	<!-- End preloader -->
	<!-- Start main section -->
	<section class="error-contents">
		<div class="container">
			<div class="row relative animated" data-animation="contentAnim" data-animation-delay="2000">
				<div class="col-md-12">
					<div class="error-title">Data Tidak Ditemukan</div>
				</div>
				<!-- End Column -->
			</div>
			<!-- End row -->
		</div>
		<!-- End container -->
	</section>
	<!-- End main section -->
	
	<!-- Start background section -->
	<section class="error-background">
		{{-- <img class="cloud-small cloud animated" src="{{ asset('assets/searching/images/cloud-1.png')}}" data-animation="fromLeft25" data-animation-delay="3000" data-xrange="20" data-yrange="20" alt="">

		<img class="cloud-big cloud animated" src="{{ asset('assets/searching/images/cloud-1.png')}}" data-animation="fromLeft8" data-animation-delay="3500" data-xrange="30" data-yrange="30" alt="">

		<img class="cloud-rain cloud animated" src="{{ asset('assets/searching/images/cloud-2.png')}}" data-animation="fromRight8" data-animation-delay="4000" data-xrange="40" data-yrange="40" alt="">
		
		<img class="rock animated" src="{{ asset('assets/searching/images/rock.png')}}" data-animation="fromRight0" data-animation-delay="600" alt="">

		<img class="sea-island animated" src="{{ asset('assets/searching/images/sea-island.png')}}" data-animation="fromLeft0" data-animation-delay="600"  alt="">

		<img class="shark animated" src="{{ asset('assets/searching/images/shark.png')}}" data-animation="fromLeft70" data-animation-delay="3200" alt=""> --}}
	</section>
	<!-- End background section -->
	
	<!-- Js files -->
	{{ HTML::script('assets/searching/js/jquery-1.11.1.min.js')}}
	{{ HTML::script('assets/searching/js/plax.js')}}
	{{ HTML::script('assets/searching/js/classie.js')}}
	{{ HTML::script('assets/searching/js/custom.js')}}


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65649206-1', 'auto');
  ga('send', 'pageview');

</script>

</body>

<!-- Mirrored from demo.themegret.com/html/island/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 11 Sep 2015 15:57:40 GMT -->
</html>