@extends('frontend.layouts.master')

@section('content')

	<div id="banner-area">
		<img src="{{asset('assets/frontend/images/banner/banner2.jpg')}}" alt ="" />
		<div class="parallax-overlay"></div>
			<!-- Subpage title start -->
			<div class="banner-title-content">
	        	<div class="text-center">
		        	<h2>Budaya Indonesia</h2>
	          	</div>
	      	</div><!-- Subpage title end -->
	</div><!-- Banner area end -->

	<section>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<!-- Blog search start -->		
					<div class="container">
						<div class="widget widget-search">
						<h3 class="widget-title">Search</h3>
						<div id="search">
							<input class="form-control form-control-lg" placeholder="search" type="search">
						</div>
					</div><!-- Blog search end -->
				</div>
			</div>
		</div>
	</section>
	
	<!-- Blog details page start -->
	<section id="main-container">
		<div class="container">
			<div class="row">
				<!-- Blog start -->
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					
					<!-- 1st post start -->
					<div class="post">
						<!-- post image start -->
						<div class="post-image-wrapper">
							<img src="{{asset('assets/frontend/images/blog/blog1.jpg')}}" class="img-responsive"  alt="" />
							<span class="blog-date"><a href="#"> {{ date('d F Y', strtotime($budaya[0]->created_at)) }}</a></span>
						</div><!-- post image end -->
						<div class="post-header clearfix">
							<h2 class="post-title">
								<a href="{{ URL::to('budaya/'.str_replace(" ", "+", $budaya[0]->Nama_Karya_Budaya)) }}">{{ $budaya[0]->Nama_Karya_Budaya }}</a>
							</h2>
							<div class="post-meta">
								<span class="post-meta-author">Posted by <a href="#"> {{ $budaya[0]->user->first_name." ".$budaya[0]->user->last_name }}</a></span>
								<span class="post-meta-cats">in <a href="#"> News</a></span>
								
							</div><!-- post meta end -->
						</div><!-- post heading end -->
						<div class="post-body">
							<p>{{ $budaya[0]->Deskripsi_Karya_Budaya }}</p>
							<br>
							<img src="{{asset('assets/frontend/images/blog/blog1.jpg')}}" class="img-responsive"  alt="" />
							<br>
								DESKRIPSI FOTO
							<br>
								<img src="{{asset('assets/frontend/images/blog/blog1.jpg')}}" class="img-responsive"  alt="" />
							<br>
								DESKRIPSI FOTO
						</div>

					</div><!-- 1st post end -->

				</div><!--/ Content col end -->
				
				<!-- sidebar start -->
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">					
					<div class="sidebar sidebar-right">
						<!-- Tab widget start-->
						<div class="widget widget-tab">
							<ul class="nav nav-tabs">
				              <li class="active"><a href="#popular-tab" data-toggle="tab">Kategori</a></li>
				              <li class=""><a href="#recent-tab" data-toggle="tab">Provinsi</a></li>
				            </ul>
				            <div class="tab-content">
				            	<div class="tab-pane active" id="popular-tab">
					                <ul class="posts-list unstyled clearfix">
					                  <li>
					                    <div class="posts-thumb pull-left"> 
					                    	<a href="#"> 
					                    		<img alt="img" src="{{ ('assets/images/blog/blog1.jpg') }}">
					                    	</a> 
					                    </div>
					                    <div class="post-content">
					                        <h4 class="entry-title"><a href="#">Bulgaria claims to find Europe's 'oldest town'</a>
					                        </h4>
					                        <p class="post-meta">
												<span class="post-meta-author">Posted by <a href="#"> Admin</a></span>
												<span class="post-meta-date"><a href="#"> <i class="fa fa-clock-o"></i> November 14</a>
												</span>
											</p>
					                    </div>
					                    <div class="clearfix"></div>
					                  </li><!-- First post end-->
					                  <li>
					                    <div class="posts-thumb pull-left"> 
					                    	<a href="#"> 
					                    		<img alt="img" src="images/blog/blog2.jpg">
					                    	</a> 
					                    </div>
					                    <div class="post-content">
					                        <h4 class="entry-title">
					                        	<a href="#">Lorem ipsum dolor sit amet, consectetur claims</a>
					                        </h4>
					                        <p class="post-meta">
												<span class="post-meta-author">Posted by <a href="#"> Admin</a></span>
												<span class="post-meta-date"><a href="#"> <i class="fa fa-clock-o"></i> October 19</a>
												</span>
											</p>
					                    </div>
					                    <div class="clearfix"></div>
					                  </li><!-- 2nd post end-->
					                  <li>
					                    <div class="posts-thumb pull-left"> 
					                    	<a href="#"> 
					                    		<img alt="img" src="images/blog/blog3.jpg">
					                    	</a> 
					                    </div>
					                    <div class="post-content">
					                        <h4 class="entry-title">
					                        	<a href="#">Should You Ever Skip Giving A Tip?</a>
					                        </h4>
					                       <p class="post-meta">
												<span class="post-meta-author">Posted by <a href="#"> Admin</a></span>
												<span class="post-meta-date"><a href="#"> <i class="fa fa-clock-o"></i> November 21</a>
												</span>
											</p>
					                    </div>
					                    <div class="clearfix"></div>
					                  </li><!-- 3rd post end-->
					                </ul>
					            </div><!-- Popular tabpan end -->

					            <div class="tab-pane" id="recent-tab">
					                <ul class="posts-list unstyled clearfix">
					                  <li>
					                    <div class="posts-thumb pull-left"> 
					                    	<a href="#"> 
					                    		<img alt="img" src="images/blog/blog3.jpg">
					                    	</a> 
					                    </div>
					                    <div class="post-content">
					                        <h4 class="entry-title"><a href="#">Bulgaria claims to find Europe's 'oldest town'</a>
					                        </h4>
					                        <p class="post-meta">
												<span class="post-meta-author">Posted by <a href="#"> Admin</a></span>
												<span class="post-meta-date"><a href="#"> <i class="fa fa-clock-o"></i> November 21</a>
												</span>
											</p>
					                    </div>
					                    <div class="clearfix"></div>
					                  </li><!-- First post end-->
					                  <li>
					                    <div class="posts-thumb pull-left"> 
					                    	<a href="#"> 
					                    		<img alt="img" src="images/blog/blog1.jpg">
					                    	</a> 
					                    </div>
					                    <div class="post-content">
					                        <h4 class="entry-title"><a href="#">Bulgaria claims to find Europe's 'oldest town'</a>
					                        </h4>
					                        <p class="post-meta">
												<span class="post-meta-author">Posted by <a href="#"> Admin</a></span>
												<span class="post-meta-date"><a href="#"> <i class="fa fa-clock-o"></i> October 19</a>
												</span>
											</p>
					                    </div>
					                    <div class="clearfix"></div>
					                  </li><!-- 2nd post end-->
					                </ul>
					            </div><!-- Recent tabpan end -->


				            </div><!-- Tab content end -->
						</div><!-- Tab widget end-->

					</div><!-- sidebar end -->
				</div>
    		</div><!--/ row end -->
		</div><!--/ container end -->
	</section><!-- Blog details page end -->
@stop