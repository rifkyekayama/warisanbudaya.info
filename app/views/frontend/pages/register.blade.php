@extends('frontend.layouts.master')

@section('content')

	<div id="banner-area">
		<img src="{{asset('assets/frontend/images/banner/banner2.jpg')}}" alt ="" />
		<div class="parallax-overlay"></div>
			<!-- Subpage title start -->
			<div class="banner-title-content">
	        	<div class="text-center">
		        	<h2>Pendaftaran | Warisan Budaya Indonesia</h2>
	          	</div>
	      	</div><!-- Subpage title end -->
	</div><!-- Banner area end -->
	
	@if($errors->has())
		<!-- Error container -->
		<div class="mb10 alert alert-danger">
			<center> {{ HTML::ul($errors->all()) }} </center>
		</div>
		<!--/ Error container -->
	@endif

	@if(Session::has('success'))
		<div class="mb10 alert alert-success">
			<center> {{ Session::get('success') }} </center>
		</div>
	@endif

	<!-- Main container start -->
	<section id="main-container">
		<div class="container">
			<center>
				<h3>Pendaftaran</h3>
				Tulis Data diri anda dibawah ini secara lengkap
			</center>
			<hr>
			{{ Session::get('error') }}
			<div class="row">
				<div class="col-md-4">
					<div class="contact-info">
						<p><i class="fa fa-info info"></i>Tulis Data diri anda secara lengkap</p>
						<p><i class="fa fa-info info"></i><font color="red">*</font> Kotak isian yang wajib diisi</p>
					</div>
				</div>

				<div class="col-md-6">
					{{ Form::open(array('url' => 'register', 'class' => 'contact-form', 'name' => 'contact-form')) }}
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Nama Depan <font color="red">*</font></label>
									{{ Form::text('nama_depan', null, array('class' => 'form-control', 'placeholder' => 'Nama Depan')) }}
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Nama Belakang <font color="red">*</font></label>
									{{ Form::text('nama_belakang', null, array('class' => 'form-control', 'placeholder' => 'Nama Belakang')) }}
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>E-Mail <font color="red">*</font></label>
									{{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => 'Email')) }}
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Password <font color="red">*</font></label>
									{{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Konfirmasi Password <font color="red">*</font></label>
									{{ Form::password('konfirmasi_password', array('class' => 'form-control', 'placeholder' => 'Konfirmasi Password')) }}
								</div>
							</div>
							
						</div>
						<div class="row">
							<div class="col-xs-8">    
								<div class="checkbox icheck">
									<label>
										{{ Form::checkbox('setuju','true') }} I agree to <a href="#">terms</a>
									</label>
								</div>                        
							</div><!-- /.col -->
							<div class="col-xs-4">
								<button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
							</div><!-- /.col -->
						</div>
					{{ Form::close() }}

					<a href="{{ URL::to('login') }}" class="text-center">Sudah menjadi member</a>
				</div>

				<div class="col-md-2">
					
				</div>

			</div>
		</div><!--/ container end -->
	</section><!--/ Main container end -->

@stop