<?php

class Budaya extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'databudaya';

	public function user(){
		return $this->belongsTo('User', 'user_id');
	}
}
