<?php

class AppController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		return View::make('frontend.pages.index');
	}

	public function cariBudaya()
	{
		$budaya = Budaya::orderBy('id','desc')->paginate(10);
		$data = [
			'budaya'	=> $budaya,
		];
		return View::make('frontend.pages.culture', $data);
	}
	
	public function lihatBudaya($judulBudaya)
	{
		$budaya = Budaya::where('Nama_Karya_Budaya','LIKE','%'.str_replace("+", " ", $judulBudaya).'%')->get();
		$data = [
			'budaya'	=> $budaya,
		];

		return View::make('frontend.pages.detailbudaya', $data);
	}
	
	public function register(){
		return View::make('frontend.pages.register');
		return Redirect::to('http://localhost:8001');
	}

	public function doRegister(){
		$rules = [
			'nama_depan' 			=> 'required|alpha',
			'nama_belakang' 		=> 'required|alpha',
			'email' 				=> 'required|email',
			'password' 				=> 'required|min:8',
			'konfirmasi_password' 	=> 'required|same:password',
			'setuju'				=> 'required',
		];

		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			return Redirect::back()->withInput()->withErrors($validator);
		}else{
			try
			{
				// Let's register a user.
				$user = Sentry::register(array(
					'first_name'	=> Input::get('nama_depan'),
					'last_name'		=> Input::get('nama_belakang'), 
					'email'    		=> Input::get('email'),
					'password' 		=> Input::get('password'),
				));

				// Let's get the activation code
				$activationCode = $user->getActivationCode();

				// Send activation code to the user so he can activate the account
				Mail::send('frontend.register.activationMail', array('nama' => Input::get('nama_depan')." ".Input::get('nama_belakang'), 'token' => $activationCode), function($message){
					$message->to(Input::get('email'))->subject('Aktivasi Pengguna');
				});

				Session::flash('success', 'Aktivasi User berhasil dikirim');
				return Redirect::to('/');
			}
			catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
			{
				Session::flash('error', 'Bidang isian email wajib diisi.');
				return Redirect::back()->withInput();
			}
			catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
			{
				Session::flash('error', 'Bidang isian password wajib diisi.');
				return Redirect::back()->withInput();
			}
			catch (Cartalyst\Sentry\Users\UserExistsException $e)
			{
				Session::flash('error', 'Pengguna sudah terdaftar.');
				return Redirect::back()->withInput();
			}
		}
	}

	public function aktivasiMember($token){
		try
		{
			// Find the user using the user id
			$user = Sentry::findUserById(User::where('activation_code','=',$token)->get()[0]->id);

			// Attempt to activate the user
			if ($user->attemptActivation($token))
			{
				// User activation passed
				Session::flash('success', 'Pengguna berhasil diaktivasi');
				return Redirect::to($_ENV['MEMBER_URL']);
			}
			else
			{
				// User activation failed
				Session::flash('error', 'Pengguna gagal diaktivasi');
				return Redirect::to('/');
			}
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			Session::flash('error', 'Pengguna tidak ditemukan');
			return Redirect::to('/');
		}
		catch (Cartalyst\Sentry\Users\UserAlreadyActivatedException $e)
		{
			Session::flash('error', 'Pengguna sudah diaktivasi sebelumnya');
			return Redirect::to('/');
		}
	}
}
