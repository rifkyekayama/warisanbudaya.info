<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	// HTML Minification
	if(App::Environment() != 'local'){
		if($response instanceof Illuminate\Http\Response){
			$output = $response->getOriginalContent();

			// Clean comments
			$output = preg_replace('/(?<!\S)\/\/\s*[^\r\n]*/', '', $output);
			// remove <!-- -->
			$output = preg_replace('/<!--(.|\s)*?-->/', '', $output);
			// remove /* */
			$output = preg_replace('!/\*.*?\*/!s', '', $output);
			//remove /*\n*\n*\n*/
			$output = preg_replace('/\n\s*\n/', '', $output);
			// remove (// comments)
			$output = preg_replace('#^\s*//.+$#m', '', $output);

			// Clean Whitespace
			$output = preg_replace('/\s{2,}/', '', $output);
			$output = preg_replace('/(\r?\n)/', '', $output);

			$response->setContent($output);
		}
	}
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('login');
		}
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| SENTRY Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('sentry_auth',function(){
	if(!Sentry::check()){
		return Redirect::to('admin/login');
	}
});
 
// digunakan untuk mengecek apakan route / url / halaman yang diakses
// di user mempunyai hak untuk mengaksesnya
Route::filter('hasAccess', function($route, $request, $value)
{
	try
	{
		$user = Sentry::getUser();
		if(!$user->hasAccess($value))
		{
			return Redirect::back();
		}
	}
	catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
	{
		return Redirect::back();
	}
  
});

Route::filter('inGroup', function($route, $request, $value) {
	try {
		$user = Sentry::getUser();
		$group = Sentry::findGroupByName($value);
		if( ! $user->inGroup($group)) {
			if(Sentry::check())
				return Redirect::to('portal')->with('message', 'No Access.');
			else
				return Redirect::to('registration#login')->with('message', 'No Access.');
		}
	} catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
		if(Sentry::check())
			return Redirect::to('portal')->with('message', 'User not found.');
		else
			return Redirect::to('registration#login')->with('message', 'User not found.');
	} catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e) {
		if(Sentry::check())
			return Redirect::to('portal')->with('message', 'Group not found.');
		else
			return Redirect::to('registration#login')->with('message', 'Group not found.');
	}
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
