<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDataBudaya extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('databudaya', function(Blueprint $table) {
            $table->increments('id');
            $table->string('point1');
            $table->string('point2a');
            $table->string('point2b');
            $table->text('point3');
            $table->text('point4');
            $table->text('point5');
            $table->text('point6');
            $table->text('point7');
            $table->text('point8');
            $table->text('point9');
            $table->text('point10');
            $table->text('point11');
            $table->text('point12');
            $table->text('point13');
            $table->text('point14');
            $table->text('point15');
            $table->text('point16');
            $table->text('point17');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('databudaya');
	}

}
