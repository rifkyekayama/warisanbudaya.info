<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('culture','WadukController@index');
Route::get('culture/detailbudaya','WadukController@detailbudaya');

Route::get('searchingculture','SearchingController@search');
Route::get('error','SearchingController@error');

Route::get('/', 'AppController@index');

Route::group(array('prefix' => 'register'), function(){
	Route::get('/', 'AppController@register');
	Route::post('/', 'AppController@doRegister');
	Route::get('aktivasi_member/{token}', 'AppController@aktivasiMember');
});
Route::get('cari_budaya', 'AppController@cariBudaya');
Route::get('budaya/{judulBudaya}', 'AppController@lihatBudaya');
